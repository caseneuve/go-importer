;; ------------ go-importer.el ------------ ;;
;; Created:     2019-01-12, 23:38    @x200
;; Last update: 2019-04-12, 18:24:21 @toshiba
;; Doc:         `go-mode' replacement for malfunctioning
;;              `go-remove-unused-imports' func.
;;              It's assumed that `gofmt' is used independently.
;; 
;; Todos:       13/01, add funcs and method checker
;;              11/04, simplify import region (beg end) etc
;;              12/04, BUG: when errors found cursor changes position
;;
;; Code starts here:

(require 'go-mode)
(require 'dash)

(defgroup go-importer-custom nil
  "Customizable `go-importer' variables"
  :group 'golang)

(defface go-importer--warning-face
  `((t :box (:color ,(face-attribute 'error :foreground))))
  "Face indicating uninstalled package"
  :group 'go-importer-custom)

;; (set-face-attribute go-importer--warning-face nil :box '(:color "#EC407A"))

(defvar go-importer--packages (go-packages-go-list)
  "A list containing all installed packages, produced by `go-packages-go-list'.")
(defvar go-importer--packages-alist nil
  "Association list of pairs where key is a package import declaration name in Go and value is the package path. Is produced by `go-importer--make-packages-alist'.")

(defun go-importer--make-packages-alist ()
  (dolist (x go-importer--packages)
    (add-to-list 'go-importer--packages-alist
                 `(,(file-name-nondirectory x) . ,x))))

(defun go-importer--get-vars ()
  (let ((vars '()))
    (save-restriction
      (when (buffer-narrowed-p)
        (widen))
      (save-excursion
        (dolist
            (regex
             '("var \\([a-zA-Z_0-9, ]*\\) [*a-zA-Z0-9. ]*$" ; var declarations
               "\\([a-zA-Z_0-9, ]*\\) := "                  ; := declarations
               "(\\([a-zA-Z_0-9, ]*\\) [*a-zA-Z0-9. ]*)")   ; func args
             vars)
          (goto-char (point-min))
          (while (re-search-forward regex nil t)
            (mapc
             (lambda (x) (add-to-list 'vars x))
             (split-string (match-string 1) ", "))))))))

(defun go-importer--update-imports ()
  "Check import declaration and definitions in the code body: if there are unused packages in imports -- delete them; if uninstalled packages are declared -- highlight them in the body."
  (unhighlight-regexp t)
  (unless go-importer--packages-alist
    (go-importer--make-packages-alist))
  (let ((buf (buffer-name))
        beg end BEG END
        pac imp path
        vars pos
        (errors '())
        (body '())
        err-re)
    (with-temp-buffer
      (insert-buffer-substring buf)
      (goto-char (point-min))
      ;; check import declaration
      ;; 1) multiline
      (cond
       ((setq beg (re-search-forward "import[[:space:]]+(" nil t))
        (setq BEG (progn (beginning-of-line) (point)))
        (setq end (1- (re-search-forward "\)" nil t)))
        (setq END (1+ end))
        (setq pac (split-string (buffer-substring-no-properties beg end)))
        (dolist (x pac imp)
          (add-to-list 'imp (file-name-nondirectory (replace-regexp-in-string "\"" "" x)))))
       ;; 2) oneliner
       ((re-search-forward "import[[:space:]]+\"" nil t)
        (setq BEG (progn (beginning-of-line) (point)))
        (while (re-search-forward "import[[:space:]]+\"" nil t)
          (add-to-list 'imp (word-at-point)))
        (forward-word-strictly)
        (setq END (1+ (point))))
       ;; 3) if none, set imp to nil
       (t (setq imp nil
                BEG (1+ (or (re-search-forward "^package [a-z]*" nil t) 0))
                END BEG)))
      ;; search for packages in code body
      (flush-lines "^[[:space:]]*//")
      (setq vars (go-importer--get-vars))
      (while (re-search-forward "[a-zA-Z0-9_]+\\.[A-Z]" nil t)
        (setq pos (point))
        (goto-char (- (point) 2))
        (backward-word 1)
        (unless (or (member (word-at-point) body)
                    (member (word-at-point) vars)
                    (looking-back "\\." (- (point) 1))) ;; don't care about methods
          (add-to-list 'body (word-at-point)))
        (goto-char pos)))
    ;; correct imports
    (unless (-same-items? imp body) ;; FIXME: what if there are non installed pkg.
                                        ; in imports AND body
                                        ; → convert this to if statement 
      ;; 1) delete unused packages from imports
      (dolist (x imp)
        (unless (member x body)
          (setq imp (delete x imp))))
      ;; 2) dodaj pominięte ścieżki w `imp' (na razie bez sprawdzania path)
      (dolist (x body)
        (unless (member x imp)
          (add-to-list 'imp x))) ;; tu: zamiast x (assoc default itd)
      ;; 3) check packages
      (dolist (x imp)
        (if (not (assoc-default x go-importer--packages-alist))
            (add-to-list 'errors x)
          (add-to-list 'path (assoc-default x go-importer--packages-alist))
          (setq imp (delete x imp))))
      ;; 4) insert corrected declarations
      (with-current-buffer buf
        (save-excursion
          (delete-region BEG END)
          (dolist (pkg path)
            (go-import-add nil pkg))
          (when errors
            (dolist (x errors)
              (if (string= x (car errors))
                  (setq err-re (concat x "\\."))
                (setq err-re (concat err-re "\\|" x "\\."))))
            (catch 'nocomment
              (while (re-search-forward err-re nil t)
                (unless (nth 4 (syntax-ppss)) ; syntax-ppss → Parse-Partial-Sexp State
                                        ; → check: parse-partial-sexp for more info
                                        ; (nth 4 = "nil if outside a comment, t if inside
                                        ; a non-nestable comment..."
                  (throw 'nocomment t))))
            ;; (backward-word-strictly 1)
            (highlight-regexp err-re 'go-importer--warning-face)
            (message "GO IMPORTER: package%s '%s' %s not installed!"
                     (if (> (length errors) 1) "s" "")
                     (mapconcat 'identity errors ", ")
                     (if (> (length errors) 1) "are" "is"))))))))

(defun go-importer-update-packages ()
  "When new package has been installed during workflow, update current list of packages"
  (interactive)
  (setq go-importer--packages (go-packages-go-list))
  (go-importer--make-packages-alist)
  (message "GO IMPORTER: package list updated!"))

;;;###autoload
(define-minor-mode go-importer
  "Check package declarations for Go lang and format them properly. Requires `go-mode'.")

;;;###autoload
(add-hook 'go-mode-hook #'go-importer)

(add-hook 'go-importer-hook
          (lambda ()
            (if go-importer
                (add-hook 'before-save-hook
                          #'go-importer--update-imports nil 'local)
              (remove-hook 'before-save-hook
                           #'go-importer--update-imports 'local))))

(provide 'go-importer)

;; go-importer.el ends here
